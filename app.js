require('node-env-file')(`${__dirname}/.env`);

// const redis = require('redis');
const createServer = require('./src/server');
const {promisify} = require('util');

const start = async () => {
  const server = await createServer(
    {
      port: process.env.PORT,
      host: process.env.HOST,
    }
  );

  await server.start();

  console.log(`Server running at: ${server.info.uri}`);
  console.log(`Server docs running at: ${server.info.uri}/documentation`);
};

process.on('unhandledRejection', (err) => {
  console.error(err);
  process.exit(1);
});

start();