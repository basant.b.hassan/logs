const {Logs}=  require('../model/Log');
const Boom = require('boom');
const main = require('../src/sendMail'),
      {getEmails} = require('./user')


/**
 * List Logs
 */
async function List(request,h){
    console.log(request.headers)
    try{
       let logs = await  Logs.find({}).select('-__v') 
        if (!logs.length) {
            throw Boom.notFound('No Logs found!');
        }     
        return h.response({ logs: logs })
    } catch (e) {
        return Boom.badImplementation(e);
    }
}

/**
 * Get Log by ID (Search)
 */
async function getById(req,h){
    try{
       let log = await  Logs.findById(req.params.id).select('-__v') 
       console.log(log) 
        
        if (!log) {
            throw Boom.notFound('log not Found!');
        }     
        return h.response({log: log })
    } catch (e) {
        return Boom.badImplementation(e);
    }
}



/**
 * POST a Log
 */
async function create (req, h){
    const log = new Logs({
    status: req.payload.status,
    title: req.payload.title,
    description: req.payload.description,
    path: req.payload.path
    });
    try{
        let Log = await log.save();
        let users = await getEmails()
        let sent = await main(Log,users);
        return h.response({ message: "Log created successfully", Log: Log })
    } catch (e) {
        return Boom.badImplementation(e);
    }
}
module.exports={
    List,
    getById,
    create
}