const {Users}=  require('../model/user');
const Boom = require('boom');
/**
 * Add email
 */
async function addEmail (req, h){
    const user = new Users({
    email: req.payload.email,
    });
    try{
        let User = await user.save();
        return h.response({ message: "User created successfully", email: User.email })
    } catch (e) {
        return Boom.badImplementation(e);
    }
}

// get All emails 
async function getEmails (){ 
    const mailList = [];
   try{
        let emails = await Users.find().select('email');
        emails.forEach(function(users){
        mailList.push(users.email);      
    });
    return mailList;
    } catch (e) {
        return Boom.badImplementation(e);
    }
}
module.exports={
    addEmail,
    getEmails 
}