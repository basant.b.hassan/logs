'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const logModel = new Schema({
  status: { type: Number, required: true, index: {unique:false}  },
  title: { type: String, required: true},
  description: { type: String, required: true},
  path: { type: String, required: true },
  created_at: { type: Date, required: true,default: Date.now() },
});

const Logs = mongoose.model('log', logModel);
module.exports.Logs = Logs;
