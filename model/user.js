'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userModel = new Schema({
  email: { type: String, required: true },
});

const Users = mongoose.model('user', userModel);
module.exports = {Users};
