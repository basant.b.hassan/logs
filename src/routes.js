module.exports = [

    './routes/logList',
    './routes/logCreate',
    './routes/logSearch',
    './routes/addEmail'
  
  ].map((elem) => require(elem));