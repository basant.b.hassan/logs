const Joi = require('joi');
const {addEmail} = require('../../controllers/user')
// const Boom = require('boom');

module.exports = {
  method: 'POST',
  path: '/email',
  options: {
    // auth: 'jwt',
    validate: {
      payload: {
        email: Joi.string().email().required().notes('user email to send logs to him'),
      },
    },
    description: 'Add Email',
    notes: 'Add Email To send Logs when it is added',
    tags: ['api'],
  },
  handler: addEmail
};