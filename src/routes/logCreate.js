const Joi = require('joi');
const {create} = require('../../controllers/log')
// const Boom = require('boom');

module.exports = {
  method: 'POST',
  path: '/create',
  options: {
    // auth: 'jwt',
    validate: {
      payload: {
        title: Joi.string().required().notes('Log Title'),
        description: Joi.string().required().notes('Log Description'),
        status: Joi.number().required().notes('Log Code Status'),
        path: Joi.string().required().notes('Log Path')
      },
    },
    description: 'Add Log',
    notes: 'Add Log to the Logs then send email to Administrator',
    tags: ['api'],
  },
  handler: create
};