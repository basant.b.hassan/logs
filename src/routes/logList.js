const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const {Logs} = require('../../model/Log');
const Boom = require('boom');
const {List} = require('../../controllers/log')

module.exports = {
  method: 'GET',
  path: '/List',
  options: {
    // auth: 'false',
    // validate: {
    //   query: {
    //     start: Joi.number().min(0).default(0).notes('Start index of results inclusive'),
    //     results: Joi.number().min(1).max(100).default(10).notes('Number of results to return'),
    //   },
    // },
    description: 'Get Logs',
    notes: 'Get List of Logs',
    tags: ['api'],
    response: {
      // StatusCode: 200,
      emptyStatusCode: 204
// 

    }
    
      // status: {
      //         success:200,
      //         // 400: validationError,
      //         // 500: error
      //     },
  
  },
  handler: List,

  // console.log(",,,,");
  // console.log(List)

};