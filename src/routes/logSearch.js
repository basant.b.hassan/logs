const Joi = require('joi');
const {getById} = require('../../controllers/log')
module.exports = {
  method: 'Get',
  path: '/search/{id}',
  options: {
    // auth: 'jwt',
    validate: {
      params:{
        id: Joi.objectId().required().notes('Id to get'),
      }

    },
    description: 'Search Log By Id',
    notes: 'Get The Log By Id',
    tags: ['api','Search'],
  },
  handler: getById
};