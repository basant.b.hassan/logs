const Hapi = require('hapi');
const jwksRsa = require('jwks-rsa');
var { mongoose } = require('../db/db');

// const Inert = require('inert');
// const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
// const Pack = require('./package');
 

const validateFunc = async (decoded) => {
  return {
    isValid: true,
    credentials: decoded,
  };
};

module.exports = async (serverOptions, options) => {
    const server = Hapi.server(
    Object.assign({
        port: 3001,
        host: 'localhost',
        routes: {
        cors: {
            origin: ['*'],
        },
        },
    }, serverOptions),
    );
    const swaggerOptions = {
        info: {
                title: 'Test API Documentation',
                // version: Pack.version,
                // endpoint: '/docs'
            },
    };

    await server.register([
    require('vision'),
    require('inert'),
    {
        plugin: HapiSwagger,
        options: swaggerOptions
    },
    // {
    //   plugin: require('lout'),
    //   options: {
    //     endpoint: '/docs',
    //   },
    // },
    {
      plugin: require('good'),
      options: {
        ops: {
          interval: 1000,
        },
        reporters: {
          consoleReporter: [
            {
              module: 'good-squeeze',
              name: 'Squeeze',
              args: [{response: '*'}],
            },
            {
              module: 'good-console',
            },
            'stdout',
          ],
        },
      },
    },
  ]);

  await server.register(require('hapi-auth-jwt2'));

  server.auth.strategy('jwt', 'jwt', {
    complete: true,
    key: jwksRsa.hapiJwt2KeyAsync({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://dev-07tb2fe6.auth0.com/.well-known/jwks.json`,
    }),
    verifyOptions: {
      audience: process.env.AUTH0_AUDIENCE,
      issuer: `https://dev-07tb2fe6.auth0.com/`,
      algorithms: ['RS256'],
    },
    validate: validateFunc,
  });
//   server.auth.default('jwt');
  const routes = require('./routes');
  server.route(routes);

//   server.route(require('./routes.js'));

  return server;
};